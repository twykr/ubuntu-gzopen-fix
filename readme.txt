=== Ubuntu gzopen() / gzopen64() Fix ===
Contributors: twykr
Tags: linux, ubuntu, 14.04, LTS, gzopen, gzopen64, fix, patch
Requires at least: 4.0
Tested up to: 4.8
Stable tag: 1.0
License: GPLv2

== Description ==

The latest versions of Ubuntu have implemented a 64-bit version of the gzopen() function, to better handle large files. Unfortunately, the Ubuntu developers have also renamed the file with no backwards-compatibility to "gzopen64()".  This means that all of the gzopen() calls in Wordpress (and related plugins) fail, such as used in Updraft Plus.

This plugin is meant to fix that, with a simple translator function call to pass gzopen() calls to gzopen64(), and lets you solve the issue without having to add this adjustment to your theme files constantly.

Should only take effect when the standard gzopen() function is not available.

== Installation ==

1. Upload folder `ubuntu-gzopen64-fix` in Wordpress plugin directory `/wp-content/plugins/`.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Move on with your life!

== Changelog ==

= 1.0 =
* Intial release
