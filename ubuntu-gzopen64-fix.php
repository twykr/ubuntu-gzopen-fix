<?php
/**
 * Plugin Name: Ubuntu gzopen64 fix
 * Plugin URI: http://wordpress.org/plugins/ubuntu-gzopen64-fix
 * Description: When activated, this plugin will put a Ubuntu ribbon on the top right corner of your website. This is a fork of Stop censorship plugin.
 * Author: Nathan Ho
 * Author URI: http://www.fiveq.com
 * Version: 1.0
 * License: GPLv2
**/

if (!function_exists('gzopen')) {
    function gzopen($filename , $mode, $use_include_path = 0 ) { 
	    return gzopen64($filename, $mode, $use_include_path);
	}
}
?>